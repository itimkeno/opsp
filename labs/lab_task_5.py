class FruitGarden:
    def __init__(self, area, numbers_of_trees, fruit_type):
        self.area = area
        self.number_of_trees = numbers_of_trees
        self.fruit_type = fruit_type

    def add_tree(self):
        pass  # Placeholder method for adding trees

    def remove_tree(self):
        pass  # Placeholder method for removing trees

    def display_info(self):
        print(f"Площадь сада: {self.area} кв.м.")
        print(f"Количество деревьев: {self.number_of_trees}")
        print(f"Тип фруктов: {self.fruit_type}")

class AppleGarden(FruitGarden):
    def __init__(self, area, numbers_of_trees):
        super().__init__(area, numbers_of_trees, "яблони")
        self.numbers_of_trees = numbers_of_trees

    def add_tree(self):
        self.numbers_of_trees += 1

    def remove_tree(self):
        if self.numbers_of_trees > 0:
            self.numbers_of_trees -= 1

class PearGarden(FruitGarden):
    def __init__(self, area, numbers_of_trees):
        super().__init__(area, numbers_of_trees, "груши")
        self.numbers_of_trees = numbers_of_trees

    def add_tree(self):
        self.numbers_of_trees += 1

    def remove_tree(self):
        if self.numbers_of_trees > 0:
            self.numbers_of_trees -= 1

# create garden objects
apple_garden = AppleGarden(600, 25)
pear_garden = PearGarden(400, 18)

# show info
print("Информация о яблоневом саде:")
apple_garden.display_info()
print("\nИнформация о грушевом саде:")
pear_garden.display_info()

# change objects
apple_garden.add_tree()
pear_garden.remove_tree()

# show info after changes
print("\nИнформация о яблоневом саде после добавления дерева:")
apple_garden.display_info()
print("\nИнформация о грушевом саде после удаления дерева:")
pear_garden.display_info()

# implicit cast to garden type, polymorphism
def describe_garden(garden):
    print(f"Этот сад содержит {garden.fruit_type}.")

describe_garden(apple_garden)
describe_garden(pear_garden)