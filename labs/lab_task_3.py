import random

def IsOddNumber(N):
    return N % 2 != 0

random_numbers = [random.randint(1, 100) for _ in range(5)]

for num in random_numbers:
    print(f"Число {num} нечетное: {IsOddNumber(num)}")