class FruitGarden:
    def __init__(self, area, number_of_trees, fruit_type):
        self.area = area
        self.number_of_trees = number_of_trees
        self.fruit_type = fruit_type

    def add_tree(self):
        self.number_of_trees += 1

    def remove_tree(self):
        if self.number_of_trees > 0:
            self.number_of_trees -= 1

    def display_info(self):
        print(f"Площадь сада: {self.area} кв.м.")
        print(f"Количество деревьев: {self.number_of_trees}")
        print(f"Тип фруктов: {self.fruit_type}")

# create garden objects
garden1 = FruitGarden(500, 30, 'Яблоки')
garden2 = FruitGarden(300, 20, 'Груши')

# show info about each garden
print("Информация о первом саде:")
garden1.display_info()
print("\nИнформация о втором саде:")
garden2.display_info()

# change objects
garden1.add_tree()
garden2.remove_tree()

# show info about each garden after changes
print("\nИнформация о первом саде после добавления дерева:")
garden1.display_info()
print("\nИнформация о втором саде после удаления дерева:")
garden2.display_info()