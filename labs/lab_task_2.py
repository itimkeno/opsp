def sort(x, y, z):
    if x > y:
        x, y = y, x
    if y > z:
        y, z = z, y
    if x > y:
        x, y = y, x
    return x, y, z

# arrange
x1, y1, z1 = 3.0, 1.5, 2.4
x2, y2, z2 = 7.5, 9.2, 8.1

# act
x1, y1, z1 = sort(x1, y1, z1)
x2, y2, z2 = sort(x2, y2, z2)

# result
print("Упорядоченный набор 1:", x1, y1, z1)
print("Упорядоченный набор 2:", x2, y2, z2)