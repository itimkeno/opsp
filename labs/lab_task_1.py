sequence = input("Введите произвольную последовательность: ")

# remove all numbers
new_sequence = ''.join([char for char in sequence if not char.isdigit()])

print("Последовательность без цифр:", new_sequence)