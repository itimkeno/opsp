import pygame
import sys
import random

# pygame initialization
pygame.init()

# colors definition
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)

snake_moods = [GREEN, BLUE, YELLOW, WHITE]

# world definition
CELL_SIZE = 10
GRID_SIZE = 40
WIDTH, HEIGHT = CELL_SIZE * GRID_SIZE, CELL_SIZE * GRID_SIZE

# window creation
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Snake")

# resources loading
eat_sound = pygame.mixer.Sound("res/eat.wav")
game_over_sound = pygame.mixer.Sound("res/game_over.wav")

class Snake:
    def __init__(self):
        self.length = 5
        self.segments = [(CELL_SIZE, CELL_SIZE) for _ in range(self.length)]
        self.direction = (CELL_SIZE, 0)
        self.segment_color = GREEN


    def move(self):
        head_x, head_y = self.segments[0]
        new_head = (head_x + self.direction[0], head_y + self.direction[1])
        self.segments.insert(0, new_head)
        self.segments.pop()

    def change_direction(self, dx, dy):
        self.direction = (dx * CELL_SIZE, dy * CELL_SIZE)

    def eat(self):
        eat_sound.play()
        self.length += 1
        self.segments.append(self.segments[-1])
        self.segment_color = snake_moods[random.randint(0, len(snake_moods) - 1)]

    def check_collision(self):
        head_x, head_y = self.segments[0]
        return (head_x < 0 or head_x >= WIDTH or head_y < 0 or head_y >= HEIGHT or
                self.segments[0] in self.segments[1:])

    def draw(self):
        for segment in self.segments:
            pygame.draw.rect(screen, self.segment_color, (*segment, CELL_SIZE, CELL_SIZE))

# snake object creation
snake = Snake()

# food object creation
food = (random.randint(0, GRID_SIZE - 1) * CELL_SIZE, random.randint(0, GRID_SIZE - 1) * CELL_SIZE)

# main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP and snake.direction != (0, CELL_SIZE):
                snake.change_direction(0, -1)
            elif event.key == pygame.K_DOWN and snake.direction != (0, -CELL_SIZE):
                snake.change_direction(0, 1)
            elif event.key == pygame.K_LEFT and snake.direction != (CELL_SIZE, 0):
                snake.change_direction(-1, 0)
            elif event.key == pygame.K_RIGHT and snake.direction != (-CELL_SIZE, 0):
                snake.change_direction(1, 0)

    snake.move()

    if snake.segments[0] == food:
        snake.eat()
        food = (random.randint(0, GRID_SIZE - 1) * CELL_SIZE, random.randint(0, GRID_SIZE - 1) * CELL_SIZE)

    if snake.check_collision():
        game_over_sound.play()
        pygame.time.delay(3000)
        snake = Snake()
        food = (random.randint(0, GRID_SIZE - 1) * CELL_SIZE, random.randint(0, GRID_SIZE - 1) * CELL_SIZE)

    screen.fill(BLACK)
    snake.draw()
    pygame.draw.rect(screen, RED, (*food, CELL_SIZE, CELL_SIZE))
    pygame.display.flip()
    pygame.time.delay(100)