import tkinter as tk
from tkinter import messagebox

def bubble_sort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return arr

def binary_search(arr, x):
    low = 0
    high = len(arr) - 1
    mid = 0
    while low <= high:
        mid = (high + low) // 2
        if arr[mid] < x:
            low = mid + 1
        elif arr[mid] > x:
            high = mid - 1
        else:
            return mid
    return -1

def sort_numbers():
    try:
        numbers = list(map(int, entry_numbers.get().split(',')))
        sorted_numbers = bubble_sort(numbers)
        label_sorted.config(text="Отсортированный список: " + ', '.join(map(str, sorted_numbers)))
    except ValueError:
        messagebox.showerror("Ошибка", "Пожалуйста, введите числа, разделенные запятыми.")

def search_number():
    try:
        number_to_find = int(entry_search.get())
        sorted_numbers = list(map(int, label_sorted.cget("text").split(': ')[1].split(', ')))
        index = binary_search(sorted_numbers, number_to_find)
        if index != -1:
            messagebox.showinfo("Результат поиска", f"Число {number_to_find} найдено на позиции {index}.")
        else:
            messagebox.showinfo("Результат поиска", f"Число {number_to_find} не найдено.")
    except ValueError:
        messagebox.showerror("Ошибка", "Пожалуйста, введите целое число для поиска.")

root = tk.Tk()
root.title("Сортировка и поиск")
root.geometry("300x200")
root.resizable(False, False)

label_numbers = tk.Label(root, text="Введите числа (разделитель запятая):")
label_numbers.pack()

entry_numbers = tk.Entry(root)
entry_numbers.pack()

button_sort = tk.Button(root, text="Сортировать", command=sort_numbers)
button_sort.pack()

label_sorted = tk.Label(root, text="Отсортированный список: ")
label_sorted.pack()

label_search = tk.Label(root, text="Введите число для поиска:")
label_search.pack()

entry_search = tk.Entry(root)
entry_search.pack()

button_search = tk.Button(root, text="Поиск", command=search_number)
button_search.pack()

root.mainloop()