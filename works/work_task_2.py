class OpenAddressingHashTable:
    def __init__(self, size):
        self.size = size
        self.table = [None] * size

    def hash_function(self, key):
        return key % self.size

    def insert(self, key, value):
        index = self.hash_function(key)
        while self.table[index] is not None:
            if self.table[index][0] == key:
                break
            index = (index + 1) % self.size
        self.table[index] = (key, value)

    def get(self, key):
        index = self.hash_function(key)
        while self.table[index] is not None:
            if self.table[index][0] == key:
                return self.table[index][1]
            index = (index + 1) % self.size
        return None

    def remove(self, key):
        index = self.hash_function(key)
        while self.table[index] is not None:
            if self.table[index][0] == key:
                self.table[index] = None
                return True
            index = (index + 1) % self.size
        return False

# hash table
hash_table = OpenAddressingHashTable(10)
hash_table.insert(1, 'A')
hash_table.insert(2, 'B')
hash_table.insert(11, 'C')

print(hash_table.get(1))  # 'A' should be printed
print(hash_table.get(11)) # 'C' should be printed

hash_table.remove(1)
print(hash_table.get(1))  # should be printed None, because element with key 1 was deleted